# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
# categorias
tecnologia = Category.create(name: "tecnologia")
liquidos = Category.create(name: "liquidos")

# 2 companies
company = Company.create(category: tecnologia, official_name: "webdox ltda", domain: "webdoxclm.com")
coca_cola = Company.create(category: liquidos, official_name: "cocacola ltda", domain: "cocacola.com")
pepsi = Company.create(category: liquidos, official_name: "pepsi ltda", domain: "pepsi.com")

# role de coca cola
trabajador_coca_cola = coca_cola.roles.create(name: "trabajador")

# departamento ti de company
ti_department = company.departments.create(name: "TI")

# roles del departamento ti de company
tl_ti_department = ti_department.roles.create(name: "Technical Lead")
backend_engineer_ti_department = ti_department.roles.create(name: "Backend Engineer")
frontend_engineer_ti_department = ti_department.roles.create(name: "Frontend Engineer")


# direcciones de company
company_address_one = company.addresses.create(name: "Santiago, providencia 171", default: true)
company_address_two = company.addresses.create(name: "Santiago, las condes rosario norte 1090")

# roles de company
representante_company = company.roles.find_by(name: "representante legal")
abogado_company = company.roles.create(name: "abogado")
human_resources_company = company.roles.create(name: "recursos humanos")
trabajador_company = company.roles.create(name: "trabajador")


# persona jhon doe
person_one = Person.create(first_name: "jhon", last_name: "doe", phone: "976123461", email: "jhondoe@gmail.com")
# persona tiene dos direcciones
address_one = person_one.addresses.create(name: "Santiago centro")
address_two = person_one.addresses.create(name: "Concepcion")

# persona tiene 2 identificaciones
identification_one = person_one.identifications.create(country_code: "CL", default: true, identification_type: :rut, identification_number: "1111111-1")
identification_one = person_one.identifications.create(country_code: "AR", identification_type: 1, identification_number: "2222124")


# la person es representante de company se agrega el rol
representante = company.entity_roles.create(role: representante_company, person: person_one, effective_from: Time.now, effective_to: Time.now + 1.year)
# se agrega la asociacion
CompanyPerson.create(person_id: person_one.id, company_id: company.id)


# tambien la persona jhon doe es un trabajador de la company coca cola
person_one_trabajador_coca_cola = coca_cola.entity_roles.create(role: trabajador_coca_cola, person: person_one, effective_from: Time.now, effective_to: Time.now + 1.year)


# la company posee una razon social la cual tiene una identificacion
razon_social = company.business_names.create(name: "razon social one")
identification_rsocial = razon_social.create_identification(country_code: "CL", identification_type: :rut, identification_number: "12212332-k")


# luego la persona tiene una relacion con la empresa coca cola
CompanyPerson.create(person_id: person_one.id, company_id: coca_cola.id)

# se crea una nueva persona 
person_two = Person.create(first_name: "juan", last_name: "perez", phone: "976123462", email: "jhondoe@hotmail.com")

# esta persona esta asociada a company pero mediante el departamento de TI
EntityRole.create(entity: ti_department, role_id: tl_ti_department.id, person_id: person_two.id)

# entonces se debe hacer la relacion entre la persona y la company, pero se agrega el departamento el cual los une
CompanyPerson.create(person_id: person_two.id, company_id: company.id, department_id: ti_department.id)

# agregar tercera persona con el rol de socio del departamento de ventas de pepsi
person_third = Person.create(first_name: "antonio", last_name: "canales", phone: "976123462", email: "ko@hotmail.com")
pepsi_ventas = pepsi.departments.find_by_name("Ventas")
socio_ventas_pepsi = pepsi_ventas.roles.find_by_name("Socio/a (Partner)")
notatio_ventas_pepsi = pepsi_ventas.roles.find_by_name("Notario/a (Notary Public)")
EntityRole.create(entity: pepsi_ventas, role_id: socio_ventas_pepsi.id, person_id: person_third.id)
CompanyPerson.create(person_id: person_third.id, company_id: pepsi.id, department_id: pepsi_ventas.id)

# agregar cuarta persona con el rol de notario del departamento de ventas de pepsi
person_fourth = Person.create(first_name: "jesus", last_name: "lopez", phone: "976123462", email: "jesus@hotmail.com")
EntityRole.create(entity: pepsi_ventas, role_id: notatio_ventas_pepsi.id, person_id: person_fourth.id)
CompanyPerson.create(person_id: person_fourth.id, company_id: pepsi.id, department_id: pepsi_ventas.id)

# agregar la cuarta persona con el rol de notario del departamento de ventas de coca cola
coca_cola_ventas = coca_cola.departments.find_by_name("Ventas")
notatio_ventas_coca_cola = coca_cola_ventas.roles.find_by_name("Notario/a (Notary Public)")
EntityRole.create(entity: coca_cola_ventas, role_id: notatio_ventas_coca_cola.id, person_id: person_fourth.id)
CompanyPerson.create(person_id: person_fourth.id, company_id: coca_cola.id, department_id: coca_cola_ventas.id)

# persona sin empresa
person_fifth = Person.create(first_name: "random", last_name: "no way", phone: "976123462", email: "random@hotmail.com")

# posibles queries
=begin
coca_cola = Company.find_by_official_name("cocacola ltda")

obtener personas del equipo de ventas de coca cola
coca_cola.search_person_by_department(department_name: "Ventas")

obtener personas del equipo de ventas de coca cola que son notarios
coca_cola.search_person_by_department(department_name: "Ventas").search_by_department_role_name(role_name: "Notario/a (Notary Public)")

obtener empresas sin representante legal
Company.where.not(id: EntityRole.select(:entity_id).joins(:role).where(roles: { name: "representante legal" }))

obtener personas sin empresas
Person.left_joins(:entity_roles).where(entity_roles: { id: nil })

obtener representantes legales de una empresa
coca_cola.legal_representatives
=end
