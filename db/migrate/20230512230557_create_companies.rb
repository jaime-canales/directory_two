class CreateCompanies < ActiveRecord::Migration[7.0]
  def change
    create_table :companies do |t|
      t.string :official_name
      t.string :domain
      t.integer :category_id
      t.text :note
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :companies, :category_id
  end
end
