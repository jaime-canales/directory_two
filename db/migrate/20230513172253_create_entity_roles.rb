class CreateEntityRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :entity_roles do |t|
      t.references :entity, polymorphic: true, null: false
      t.references :person, null: false, foreign_key: true
      t.references :role, null: false, foreign_key: true
      t.datetime :effective_from
      t.datetime :effective_to
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
