class CreateRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :roles do |t|
      t.string :name
      t.references :roleable, polymorphic: true, null: false
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
