class CreateAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :addresses do |t|
      t.string :name
      t.references :addresable, polymorphic: true, null: false
      t.boolean :default, default: false
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
