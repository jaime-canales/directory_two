class CreateIdentifications < ActiveRecord::Migration[7.0]
  def change
    create_table :identifications do |t|
      t.references :identificable, polymorphic: true, null: false
      t.string :country_code
      t.integer :identification_type
      t.string :identification_number
      t.boolean :default, default: false
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
