class CreateBusinessNames < ActiveRecord::Migration[7.0]
  def change
    create_table :business_names do |t|
      t.integer :company_id
      t.string :name
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :business_names, :company_id
  end
end
