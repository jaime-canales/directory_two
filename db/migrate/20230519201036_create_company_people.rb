class CreateCompanyPeople < ActiveRecord::Migration[7.0]
  def change
    create_table :company_people do |t|
      t.integer :person_id
      t.integer :company_id
      t.integer :department_id
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :company_people, :person_id
    add_index :company_people, :company_id
    add_index :company_people, :department_id
  end
end
