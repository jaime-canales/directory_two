# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_05_19_201036) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "name"
    t.string "addresable_type", null: false
    t.bigint "addresable_id", null: false
    t.boolean "default", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["addresable_type", "addresable_id"], name: "index_addresses_on_addresable"
  end

  create_table "business_names", force: :cascade do |t|
    t.integer "company_id"
    t.string "name"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_business_names_on_company_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string "official_name"
    t.string "domain"
    t.integer "category_id"
    t.text "note"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_companies_on_category_id"
  end

  create_table "company_people", force: :cascade do |t|
    t.integer "person_id"
    t.integer "company_id"
    t.integer "department_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_company_people_on_company_id"
    t.index ["department_id"], name: "index_company_people_on_department_id"
    t.index ["person_id"], name: "index_company_people_on_person_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.integer "company_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_departments_on_company_id"
  end

  create_table "entity_roles", force: :cascade do |t|
    t.string "entity_type", null: false
    t.bigint "entity_id", null: false
    t.bigint "person_id", null: false
    t.bigint "role_id", null: false
    t.datetime "effective_from"
    t.datetime "effective_to"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["entity_type", "entity_id"], name: "index_entity_roles_on_entity"
    t.index ["person_id"], name: "index_entity_roles_on_person_id"
    t.index ["role_id"], name: "index_entity_roles_on_role_id"
  end

  create_table "identifications", force: :cascade do |t|
    t.string "identificable_type", null: false
    t.bigint "identificable_id", null: false
    t.string "country_code"
    t.integer "identification_type"
    t.string "identification_number"
    t.boolean "default", default: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["identificable_type", "identificable_id"], name: "index_identifications_on_identificable"
  end

  create_table "people", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "email"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "roleable_type", null: false
    t.bigint "roleable_id", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["roleable_type", "roleable_id"], name: "index_roles_on_roleable"
  end

  add_foreign_key "entity_roles", "people"
  add_foreign_key "entity_roles", "roles"
end
