class Identification < ApplicationRecord
  acts_as_paranoid
  VALID_IDENTIFICABLE_TYPES = %w[BusinessName Person].freeze
  belongs_to :identificable, polymorphic: true
  enum identification_type: { rut: 0, dni: 1 }.freeze, _prefix: :identification_type
  validates :identificable_type, allow_nil: false, inclusion: { in: VALID_IDENTIFICABLE_TYPES, message: :invalid }

  before_save :update_is_default

  private

  def update_is_default
    return unless default?
    return if identificable.class.name != "Person"

    identification_ids = self.class.where(identificable: identificable).ids - [id]
    self.class.where(id: identification_ids).update_all(default: false)
  end
end
