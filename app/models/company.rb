class Company < ApplicationRecord
  acts_as_paranoid
  include HasDefaultAddress
  has_many :addresses, as: :addresable, dependent: :destroy
  has_many :roles, as: :roleable, dependent: :destroy
  has_many :entity_roles, as: :entity, dependent: :destroy

  has_many :company_persons, dependent: :destroy
  has_many :persons, through: :company_persons
  belongs_to :category
  has_many :departments, dependent: :destroy
  has_many :business_names, dependent: :destroy

  before_validation :normalize_official_name, :normalize_domain

  validates_presence_of :official_name
  validates :official_name, :domain, uniqueness: true

  validates_as_paranoid
  validates_uniqueness_of_without_deleted :official_name, :domain, case_sensitive: false
  delegate :search_person_by_department, to: :persons

  def legal_representatives
    legal_id = roles.find_by_name("representante legal").id
    persons.joins(:entity_roles, :roles).where(entity_roles: { entity: self }).where(roles: { id: legal_id })
  end
=begin
  def persons
    method to get all persons without company person model
    company_roles = EntityRole.where(entity_type: "Company", role_id: roles.ids).pluck(:person_id)
    department_roles = EntityRole.where(entity_type: "Department", entity_id: departments.ids).pluck(:person_id)
    Person.where(id: [company_roles + department_roles])
  end
=end
  DEFAULT_DEPARTMENTS = [
    "Administracion",
    "Asuntos legales",
    "Atencion al cliente y servicio post-venta",
    "Compras y adquisiciones",
    "Contabilidad",
    "Control de calidad",
    "Desarrollo de negocios",
    "Direccion general",
    "Finanzas",
    "Gestion de riesgos y cumplimiento",
    "Producto",
    "Investigacion",
    "Logistica y cadena de suministro",
    "Mantenimiento y reparación de equipos",
    "Marketing y publicidad",
    "Presupuesto y control financiero",
    "Producción y operaciones",
    "Recursos humanos",
    "Seguridad informática",
    "Soporte tecnico",
    "Tecnologia de la informacion (TI)",
    "Tesoreria",
    "Ventas",
    "Otra area"
  ].freeze

  DEFAULT_COMPANY_ROLES = [
    "representante legal",
    "contacto"
  ].freeze

  DEFAULT_DEPARTMENTS_ROLES = [
    "Abogado/a (Lawyer)",
    "Asociado/a Legal (Legal Associate)",
    "Asesor/a Jurdico/a (Legal Counsel)",
    "Socio/a (Partner)",
    "Especialista en Propiedad Intelectual",
    "Asistente Legal (Legal Assistant)",
    "Secretario/a Legal (Legal Secretary)",
    "Investigador/a Jurídico/a (Legal Researcher)",
    "Notario/a (Notary Public)",
    "Paralegal",
    "Abogado/a Corporativo/a (Corporate Lawyer)",
    "Especialista en Derecho Laboral (Employment Law Specialist)",
    "Especialista en Derecho de Familia (Family Law Specialist)",
    "Especialista en Derecho Penal (Criminal Law Specialist)",
    "Especialista en Derecho Ambiental (Environmental Law Specialist)",
    "Otros"
  ].freeze

  after_create :initialize_default_company_roles, :initialize_default_departments, :initialize_default_department_roles

  private

  %w[official_name domain].each do |attr|
    define_method("normalize_#{attr}") do
      self.send("#{attr}=", send(attr).downcase) if send(attr).present?
    end
  end

  def initialize_default_company_roles
    sql = "INSERT INTO roles(name, roleable_type, roleable_id, created_at, updated_at) VALUES "
    sql_values = DEFAULT_COMPANY_ROLES.each_with_object([]) do |name, arr|
      arr << "('#{name}', 'Company', #{id}, current_timestamp, current_timestamp)"
    end
    sql += sql_values.join(", ")
    ActiveRecord::Base.connection.insert(sql)
  end

  def initialize_default_departments
    sql = "INSERT INTO departments(name, company_id, created_at, updated_at) VALUES "
    sql_values = DEFAULT_DEPARTMENTS.each_with_object([]) do |name, arr|
      arr << "('#{name}', #{id}, current_timestamp, current_timestamp)"
    end
    sql += sql_values.join(", ")
    ActiveRecord::Base.connection.insert(sql)
    departments.ids
  end

  def initialize_default_department_roles
    departments.ids.each do |department_id|
      sql_values = DEFAULT_DEPARTMENTS_ROLES.each_with_object([]) do |name, arr|
        arr << "('#{name}', 'Department', #{department_id}, current_timestamp, current_timestamp)"
      end
      sql = "INSERT INTO roles(name, roleable_type, roleable_id, created_at, updated_at) VALUES "
      sql += sql_values.join(", ")
      ActiveRecord::Base.connection.insert(sql)
    end
  end
end
