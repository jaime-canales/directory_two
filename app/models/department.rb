class Department < ApplicationRecord
  acts_as_paranoid
  belongs_to :company
  has_many :roles, as: :roleable, dependent: :destroy
  has_many :entity_roles, as: :entity, dependent: :destroy

  validates_as_paranoid
  validates :name, presence: true
  validates_length_of :name, maximum: 255
  validates_uniqueness_of_without_deleted :name, scope: :company_id, case_sensitive: false
end
