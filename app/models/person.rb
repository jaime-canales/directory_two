class Person < ApplicationRecord
  acts_as_paranoid
  include HasDefaultAddress
  has_many :addresses, as: :addresable, dependent: :destroy
  has_many :identifications, as: :identificable, dependent: :destroy
  has_one :default_identification, -> { where(default: true) }, as: :identificable, class_name: "Identification"

  has_many :entity_roles, class_name: "EntityRole", dependent: :destroy
  has_many :roles, through: :entity_roles, source: :role

  has_many :company_entity_roles, -> { where(entity_type: "Company") }, class_name: "EntityRole"
  has_many :company_roles, through: :company_entity_roles, source: :role

  has_many :company_persons, dependent: :destroy
  has_many :companies, through: :company_persons

  has_many :department_entity_roles, -> { where(entity_type: "Department") }, class_name: "EntityRole"
  has_many :department_roles, through: :department_entity_roles, source: :role

  has_many :departments, through: :company_persons

  validates_as_paranoid
  validates :email, :first_name, :last_name, presence: true
  validates_uniqueness_of_without_deleted :email, :first_name, :last_name, case_sensitive: false

  scope :search_person_by_department, ->(department_name:) { joins(:departments).where(departments: { name: department_name}).distinct }

  scope :search_by_department_role_name, ->(role_name:) { includes(:roles).where(roles: { name: role_name }) }
=begin
  method to get all companies without company_person model
  def companies
    company_roles = EntityRole.where(entity_type: "Company", person_id: id).pluck(:entity_id)
    department_roles = EntityRole.includes(role: :roleable).where(entity_type: "Department", person_id: id).map(&:role).map(&:roleable).pluck(:company_id)
    Company.where(id: [company_roles + department_roles])
  end
=end
end
