class Address < ApplicationRecord
  acts_as_paranoid
  VALID_ADDRESABLE_TYPES = %w[Company Person].freeze
  belongs_to :addresable, polymorphic: true
  validates :addresable_type, allow_nil: false, inclusion: { in: VALID_ADDRESABLE_TYPES, message: :invalid }

  before_save :update_is_default

  private

  def update_is_default
    return unless default?

    address_ids = self.class.where(addresable: addresable).ids - [id]
    self.class.where(id: address_ids).update_all(default: false)
  end
end
