class Role < ApplicationRecord
  acts_as_paranoid
  VALID_ROLEABLE_TYPES = %w[Company Department].freeze
  belongs_to :roleable, polymorphic: true
  has_many :entity_roles
  has_many :company_entities, through: :entity_roles, source: :entity, source_type: "Company"
  has_many :department_entities, through: :entity_roles, source: :entity, source_type: "Department"
  validates :roleable_type, allow_nil: false, inclusion: { in: VALID_ROLEABLE_TYPES, message: :invalid }

  validates_as_paranoid
  validates :name, presence: true
  validates_length_of :name, maximum: 255
  validates_uniqueness_of_without_deleted :name, scope: %i[roleable_id roleable_type], case_sensitive: false
end
