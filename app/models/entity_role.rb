class EntityRole < ApplicationRecord
  acts_as_paranoid
  validates_presence_of :person_id
  belongs_to :entity, polymorphic: true
  belongs_to :person
  belongs_to :role
  validates_as_paranoid
  validates_uniqueness_of_without_deleted :person_id, scope: %i[entity_type entity_id role_id]

  validate :only_one_department_role_per_company, if: -> { entity_type == "Department" && person_id.present? }
  validate :only_one_role_per_company, if: -> { entity_type == "Company" && person_id.present? }

  def only_one_department_role_per_company
    department_role_ids = person.roles.where(roleable_type: "Department").pluck(:roleable_id)
    company_department = Department.find_by(id: department_role_ids, company_id: entity.company_id)

    person_role = person.roles.where(roleable_type: "Department").find_by(roleable_id: company_department&.id)&.name

    return unless company_department

    errors.add(:base, "Person already has a department #{company_department.name} and role #{person_role} for company #{entity.company.official_name}")
  end

  def only_one_role_per_company
    current_role = person.entity_roles.find_by(entity: entity)
    errors.add(:base, "Person already has a #{current_role.role.name} role for company #{entity.official_name}") if current_role
  end
end
