module HasDefaultAddress
  extend ActiveSupport::Concern

  included do
    has_one :default_address, -> { where(default: true) }, as: :addresable, class_name: "Address"
  end
end
