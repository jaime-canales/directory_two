class CompanyPerson < ApplicationRecord
  acts_as_paranoid
  belongs_to :company
  belongs_to :person
  belongs_to :department, optional: true
end
